#!/bin/bash

mkdir reports

export IGNITRON_OPTS="-Xmx14g"

run() {
  cd "$1"
  ignitron --pullrequest "$2" --basebranch "$3"
  cd ..
}

count=0
while read line
  do
    directory=$(echo "${line}" | cut -d ',' -f 1)
    changes=$(echo "${line}" | cut -d ',' -f 2)
    base=$(echo "${line}" | cut -d ',' -f 3)
    run "${directory}" "${changes}" "${base}" > "reports/$directory.$count"
    echo "Finished peforming experiment ${directory}.${count}!"
    count=$(( ($count + 1) % 10 ))
done < commits.csv
