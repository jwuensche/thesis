#!/bin/zsh

repos=(guava javaparser junit5 mockito jackson-databind)

for repo in ${repos[@]}
do
  simSeq=0
  seqLen=0.0
  highSim=0
  highLen=0.0
  for file in reports/$repo.*
  do
    simSeq=$(( $simSeq + $(cat $file | jq '.warnings[].reason' | grep Sequence | wc -l) ))
    seqLen=$(( $seqLen + $(cat $file | jq '.warnings | map(select(.reason == "Similar Sequence")) | .[].changes_content' | sed 's/\\n/\n/g' | wc -l) ))
    highSim=$(( $highSim + $(cat $file | jq '.warnings[].reason' | grep High | wc -l) ))
    highLen=$(( $highLen + $(cat $file | jq '.warnings | map(select(.reason == "High Method Similarity")) | .[].changes_content' | sed 's/\\n/\n/g' | wc -l) ))
  done 
  echo "$repo,0,$simSeq,$highSim,$(( ($seqLen  * 100) / ($simSeq * 100)  )),$(( ($highLen * 100 ) / ($highSim * 100)  ))"
done
