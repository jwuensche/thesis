#!/bin/bash

# This script strips a repository of the main branch and puts all unique commits into different branches in the order they occured in.
# They can the be compared to another to find changes in the branches.

git log --pretty='%H' | while read hash; do git branch "${hash}"; done
git checkout $(git branch | head -n 1)
git branch -d main
