# Conclusion

In this thesis we proposed a technique for the detection and treatment of similar test cases and test code based on the usage of abstract syntax trees.
We focus our research on the application in forked software system environments, its goal is by integrating in the pull request review process to prevent the introduction of these similar test code in future code additions and automatic treatment of patterns introduced by new tests.
The core concepts of this technique are the usage of tree matching on trees with generalized feature such as identifier names, comments, and literals to find common subtrees to added and changed methods.
These common subtrees can then be used depending on the type of the root of the subtree.
We propose the detection, unification, and extraction of equal test case methods, common sequence of statements over multiple methods, and a general similarity value measured by the size of common subtrees in comparison to the root node of test methods.

We have implemented a prototype to demonstrate this technique on Java test code and performed an evaluation on 50 recent pull requests of five popular Java repositories containing changes to any test case.
Most cases representing some kind of duplication or similarity were included, indicating that the number of false negatives is low. In comparison some false positives were contained, depending on the workflow for testing used in the repository these cases were more or less widespread. In reaction to this we have discussed solutions to handle these methods, including to classify method similarity based on the context they are enclosed in.
Testing has shown that the context in which methods are defined is most often the cause of these false positives, for example, some methods were more than once defined in their present form in the object oriented approach of some testing environments.
The automated test unification implemented in our prototype has been used seldomly in practice. Only a few cases have fulfilled the strict requirements, though this was to be expected as named in the \autoref{evaluation}.
In general, these results met our expectations and confirmed the viability of the AST-based test similarity and the prospect of automated alteration such as merging of cases.
Topics left to be covered in the implementation is the extraction of sequences and better classification of the context of methods, some challenges were left in this regard and can be completed in future work.

## Future Work

As discussed in \autoref{evaluation}, some areas of the implementation can be enhanced to better the construction of automated changes and reduce the number of false positives.
For the latter, we propose the inclusion of context to methods, this would include the class structure in which the method is in included, the given annotations if any are available, type resolution, and method signature.
Especially, the introduction of proper type resolution would need the addition of a type resolver, which has been so far not added to the prototype, with it purely depending on the parsing of source code.
Also, this addition would come with the added benefit of determining the definite type used in statement sequences, which would open up the opportunity to extract them, if common in more than one place.

Additionally, to improve the amount of false positives the testing environment used could come into consideration, for instance, in \emph{Mockito} tests do not carry annotations as they do for example in \emph{Guava}. 
Or, member methods, which have been found in \emph{Jackson-Databind} for example, do not carry an `@Test` annotation or similar as test cases, but this is not universal for repositories and has to be determined on repository basis.
The detection of this and handling is left open in our concept.

Furthermore, the prototype implementation only works on Java source code, though the technique presented can be applied to any given language.
Testing the viability of it in other languages like C, Rust, Typescript etc. would give some useful insights on how to better deal with other test environments.

Finally, the number of pull requests used for evaluation is rather low as they had to be evaluated by hand, increasing the count of projects and also including more projects in different levels of maturity would offer a more variance rich outlook on how we perform in general on repositories.
