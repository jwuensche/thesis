# Evaluation

In this section we describe the scope, setup, and dataset of our analysis, followed by the observed results and their evaluation to our expectations.
Next, we interpret our results and formulate potential downsides that our results show, as well as technical limitations.
Lastly, we name threats to validity of the here performed study.

## Scope and Reason

In our evaluation we analyze which structures of selected pull requests can be detected dependably and which structures are falsely reported or modified.
For the generated results, we check for each the validity of their detected locations and their accuracy in context of the software project under study.
As a general indicator on found results, we summed the number of occurrences in \autoref{table:result} and \autoref{table:length} up. We point out cause of irregularities and the general distribution of found similarities, with reference to their respective section in the more detailed analysis of singular projects.
Moreover, for a more detailed view on results, we show examples which demonstrate negative and positive characteristics of found results best and discuss to which degree they are incorrect or inaccurate.

## Setup

We run our analysis on a virtual machine running on a second generation AMD EPYC processor running at 2.5 GHz having access to 8 cores, and 16 GB of RAM.
As underlying operation system we have chosen Debian Buster with the Linux 4.19 LTS kernel, and openjdk 11.0.9.1 as found in their repositories.
All experiments are run consecutively without interruptions. The execution time reported is measured via the UNIX `time` tool.

## Dataset

We found no available dataset we could use, fitting to our analysis cases, therefore we created our own dataset using popular open source repositories.
To gather data of popular repositories we used the statistics of \emph{mvnrepository},\footnote{\url{https://mvnrepository.com/popular}} and picked fitting popular repositories, based on the existence of a public followable pull request workflow and if they introduce new features readily and reliably with tests.
By applying these restrictions, we have found \emph{Guava},\footnote{\url{https://github.com/google/guava}} \emph{Mockito},\footnote{\url{https://github.com/mockito/mockito}} \emph{Jackson-Databind},\footnote{\url{https://github.com/FasterXML/jackson-databind}} \emph{Junit5},\footnote{\url{https://github.com/junit-team/junit5}} and \emph{Javaparser}\footnote{\url{https://github.com/javaparser/javaparser}} as valid candidates.

\begin{table}[h]
    \centering
    \caption{Excerpt of the dataset of pull requests}
    \resizebox{\textwidth}{!}{
    \begin{tabular}{lrr}
    \toprule
        Repository & Commit Merged PR & Base Commit \\
        \midrule
        junit5 & 1c8da0d21ee5a70c1766c5b9c00985d5e64b8ad5 & a37f726bc3f5241cdffd7491e16b0266002e0eb9 \\
        & 5067d620a37c1cb12fcac2e5ef5eced3db36a484 & 1ae3b4081441737fd15db831015fbaf557274a48 \\
        & 8760ec1e67b4a27b1c2c699721a312651df32e99 & 925cfbd710aafb885f17ff097847719f5be12f97 \\
        & 9d8da988898c994d65ac38b53df15d0ff3757d74 & 172d30ba68efb5812c4f52c342a9be8ffd53434c \\
        & 7457e49d8efd0c7f2671fee44120cf269b9c92d2 & 9e254c1d92552e2e85b3abc174f96e08c7972eb0 \\
        \bottomrule
    \end{tabular}}
    \label{table:excerpt}
\end{table}

The dataset is structured by defining for each pull request a \emph{Changes Commit} and a \emph{Base Commit}. Whereas the \emph{Changes Commit} represents the last rebased commit or the merge commit, depending on the repository and the \emph{Base Commit} the last commit before the pull request was introduced to the branch. An example can be seen in \autoref{table:excerpt} which shows five pull requests from the \emph{Junit5} project.

As we manually evaluate pull requests, we restricted the number of pull requests for each repository to ten for each of our five test projects, which gives us 50 pull requests in total to perform our analysis on.
Again, we apply some criteria to find fitting pull requests in the available set of commits.
First, a pull request has to modify the content of at least one test method in its commit history.
Second, the pull request need to been merged, pull request that were in the moment of our gathering of data in staging or rejected are omitted.
Lastly, the most recent pull requests are chosen, meaning the 10 commits in the dataset are all the most recent ones fitting our criteria.
All data in the dataset has been collected on the 11th of December 2020 and can be found in the appendix of the thesis.

\begin{table}
  \centering
  \caption{Tests and SLOC of evaluated projects}
  \begin{tabular}{lrr}
  \toprule
    Project & Number of tests & SLOC \\
    \midrule
    jackson-databind & 2667 & 198650 \\
    mockito & 2018 & 88851 \\
    junit5 & 3653 & 134727 \\
    javaparser & 2371 & 272627 \\
    guava & 1071 & 756069 \\
    \bottomrule
  \end{tabular}
  \label{table:projects}
\end{table}

In \autoref{table:projects} we list the number of tests and Source Lines Of Code (SLOC) contained in each repository, to give an rough overview of the size of each project.
The most tests can be found in \emph{Junit5}, this is partially due to the structure of tests they created, for instance, they define multiple empty test cases to test if the test case name or annotation combination is successfully interpreted, as they use their own testing framework in their tests.
The least test cases are defined in \emph{Guava}, though more code has been used to extract code from test cases to their own classes where behavior used in tests is defined.

## Format of results

To analyze the performance of our prototype implementation, we check the generated output of the program specified in \autoref{code:format} to our expectation and validate the output itself on its sensibility.
As the output is json based, we filter using \emph{jq} \footnote{\url{https://stedolan.github.io/jq/}} to select the output for each individual cause a waning or modification has been reported for.
We do not use the automated modification feature, as to preserve a clean tree in our tests, but evaluate the output containing the planned modifications and base our judgment on that.

## Results

\begin{table}
  \centering
  \caption{Results of the case study performed in \emph{Number of Occurrences}}
  \begin{tabular}{lrrr}
  \toprule
      Repository & Merged Methods & Sequences & High Method Similarity \\
      \midrule
      jackson-databind & 0 & 11 & 559 \\
      mockito & 0 & 30 & 55 \\
      junit5 & 0 & 10 & 2412 \\
      javaparser & 0 & 69 & 9 \\
      guava & 0 & 18 & 154 \\
      \bottomrule
  \end{tabular}
  \label{table:result}
\end{table}

\begin{table}
    \centering
    \caption{Average number of lines in each individual category of matches}
    \begin{tabular}{lrr}
    \toprule
        Repository & Avg. lines in Sequence & Avg. lines in Methods \\
        \midrule
        jackson-databind & 3.0 & 4.43 \\
        mockito & 3.0 & 11.71 \\
        junit5 & 3.39 & 3.11 \\
        javaparser & 3.0 & 11.34 \\
        guava & 3.0 & 3.06 \\
        \bottomrule
    \end{tabular}
  \label{table:length}
\end{table}

For an initial overview, \autoref{table:result} and \autoref{table:length} contain a numerical summary of the found results.
We can see in \autoref{table:result}, that a relative constant amount of sequences are detected over all repositories in our dataset, ranging from 10 to 69 sequences for each repository in total over all their commits.
A greater fluctuation of values can be observed in the amount of methods with high similarity, some projects like \emph{Javaparser} or \emph{Mockito} only contain a relatively low amount of detected methods, while others detect a very high amount of method similarity as in \emph{Junit5}.
To find the cause of this we explore the generated results in more detail in \autoref{mockito}, \autoref{junit5}, and \autoref{javaparser}.

We show in \autoref{table:length} the average number of lines a detected sequence or method consists of in each repository. It can be seen, that in repositories which have the lowest amount of similar methods, those which are included are on average longer.
In further analysis (\autoref{mockito} and \autoref{javaparser}) they have shown to deliver better results overall.
