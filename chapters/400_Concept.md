# AST-based Test Similarity

In this section, we describe the processes of our technique of \gls{AST}-based Test Similarity and our intentions and rationale behind them.
We start with a short overview of the complete process itself and split this process down into more in-depth explained steps afterwards.
Moreover, the choice of means to achieve a desired goal are discussed respectively.

## Overview

\begin{figure}[t]
  \includegraphics[width=\textwidth]{images/overview.pdf}
  \caption[Technique Overview Chart]{Overview chart, showing the process of parsing code to the finished result}
  \label{figure:overview}
\end{figure}

\autoref{figure:overview} shows a rough overview of the complete process encompassed by our technique to find equivalent sequences and methods in the provided source code versions.
Version \emph{Base} and \emph{Fork} represent different states of the same source code repository, we generally refer to them in explanations as \emph{Base Branch} and \emph{Changes Branch} respectively to better integrate their usage in the planned area of application in the pull request review workflow.
The \emph{Base Branch} represents the state of the code the pull request is requested to be applied on. While the \emph{Changes Branch} consists of the code changes added by the pull request itself.

In the beginning of the process, a selection and generalization of methods is performed, to reduce the amount of comparisons required to a minimum and enable detection of code clones of up to type-2.
This selection is composed of a pre-filtering, to find modified files in the changes introduced by the pull request, and a filtering, to exclude methods left unmodified from consideration, reducing the effort required for more complex comparisons.
After this filtering is completed, a generalization of methods is applied on methods.
Additionally, files are parsed in this stage and subtrees, representing method declarations, are extracted.

After these first steps, the matching of methods is executed, with the goal of finding the greatest common subtrees.
These comparisons are done under different degrees of tree generalizations, as different levels allow for certain clone types or patterns to be detected, enabling a range of possible modifications to be found later on.
The resulting set of pairs of matched methods with their isomorphic subtrees are then given to the next element of the pipeline, which filters pairs of methods on characteristics, concerning the type of the common subtrees found.
First, method pairs are chosen which are generalized up to a level of type-2 clones and contain any kind of statement in their shared trees.
In these, similar sequences are searched and the maximum possible length of contained sequences is determined.
Second, method pairs with generalization applied about equal to type-2 clones, with the exemption of the omission of literals, which contain at least one equal block in their found subtrees, are examined, searching for methods that can be potentially treated as equals.
Third, for method pairs, which are generalized equivalently to type-2 clones. All method pairs are considered calculating a similarity value based on the size of contained subtrees, if this size lies above a certain threshold, the methods are presumed to be of high similarity.

Each of these pipelines will create a report and possible changes, all created reports are then combined into one complete report which is then given as the output of the process to inform users of the changes done and possible bad patterns found (given as warnings).
