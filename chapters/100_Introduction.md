# Introduction

The field of software engineering aims to optimize the implementation and design of software \cite{ieeeDefinitions}.
To this end many studies have been conducted searching for possibilities of better code organization, for example, moving from a purely imperative development pattern to object-oriented, functional programming, or a combination of these, fitting the paradigm of programming to the thought process of the developer.
Additionally to these fundamentals, part of software engineering in practice is the provisioning of aid to developers during the development in existing structures, helping to improve code quality.
To attain this goal, another part of software engineering is the automated evaluation of code and the automated application of strategies and methods to improve it.
Hence minimizing time spent on refactoring from developers, who may take more time for the same result. Since they have to carefully check source code to fulfill certain criteria or to not violate constraints in the programs logic.
Since the majority of fixes and problems, which are required to be detected and fixed often over the entire code base, include rather simple structures, such as prohibiting access to invalid values (e.g. an access to null values in certain languages) or the omission of cases in checks (e.g. ranges or variants).

Performing these improvements regularly leads to detecting errors early, subsequently preventing the need for later fixes, and by that reducing costs and effort required in the future \cite{barry1981software}.
Automating these fixes can support the early detection of undesired behavior, by providing an easily accessible service to spotlight problems in the code.
Content of research in software engineering has been for a while the automated detection and treatment of certain bad practices \cite{zheng2006value, kataoka2001automated}.
Multiple techniques have partially found wide use in the software development workflow, such as code linters that look for patterns of code smells in source code and suggest better alternatives or apply them by themselves.
Often not included in code linters is the detection of code clones, since these can be difficult to detect and generally require more computational effort.
Clone detection tools like dup \cite{baker1995finding}, NiCAD \cite{cordy2011nicad}, CloneDR \cite{baxter1998clone}, CCFinder \cite{kamiya2002ccfinder}, and others have been developed, showing that automated detection of clones to a certain degree is possible and can improve the understanding of the code base as well as its maintainability.

Another topic of interest in recent research has been the measuring of test similarity and the optimization of test suites to be more efficient and maintainable.
Part of this has been to evaluate the quality of test suites or to select certain test cases to speed up the testing process and give developers better feedback times \cite{neto2018ci}.
The general use-case of code clone detection may be extrapolated and modified to be applied on test case similarity.
However, most tools and research to measure test similarity, we have found, are based on the practice of stack trace comparisons, program profiling, or code coverage of test cases.
While these offer some good insights, the extendability on automated code change recommendations is low as they do not interpret the actual code written but the code's observable effects.
Combining the test similarity measure with static analysis on abstract syntax trees, allows for the alteration of source code to fix unwanted behavior, which would prove more difficult with other approaches to test similarity measuring.

## Goal of this Thesis

In this thesis we address the determination of test case similarity and their automated treatment by proposing a technique to find similar test cases by using static analysis on abstract syntax trees and modification of found similarities in abstract syntax trees by extracting or unifying common sub trees.
Our technique is designed to work on changing source code of software projects using the \emph{Git} \gls{VCS}.
In our concept we describe how to implement this on common git workflows.

Additionally we provide a prototype implementation to demonstrate our technique.
While the implementation of our technique can be done for any language, we have chosen Java test code to be our object of analysis.
For an estimation of potential of our technique, we perform an evaluation on recent commits of popular Java open source repositories.

## Structure of this Thesis

This thesis starts with an overview of the technologies and theoretical basis used or related to our topic, as well as definitions on used terms in \autoref{background}.
Following, we give an insight into \nameref{related-work} and research in the specific topic of AST-based Code Similarity, automated patching, and Test Analysis.
After this, we present our technique of an \nameref{ast-based-test-similarity}, explaining the core concepts and the integration into an exemplary Forking Workflow.
Next, in Chapter \nameref{implementation}, we describe our prototype in detail with all functions of the concept implemented.
Finally, we explain the setup and creation of our \nameref{evaluation} and show an overview of results, followed by details to certain repositories and interpretation of results.
Lastly in our \nameref{conclusion}, we formulate found information and knowledge gained, as well as topics left open for future work.
