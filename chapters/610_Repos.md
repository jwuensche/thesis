### Jackson-Databind

The comparisons on the \emph{jackson-databind} repository shows over 500 method matches having a higher similarity than 95%, this is due to the structure of jackson test, they use inherited classes which are then used in separate test methods. The found highly similar methods are part of these objects as common methods such as getters and setters were duplicated through out the code base. Some of these can be solved by introducing a class abstracting this from all the lower classes, though extracting sensible abstractions to put into one class to be inherited by more than two classes is seldomly the case. Explaining the existence of the found matches in our study.

\begin{figure}[t]
    \lstinputlisting[language=java, label={code:failure}, caption=Methods referenced in false match in jackson-databind commit]{code/error.java}
\end{figure}

We also observed some error prone matches in these scenarios which can be seen in \autoref{code:failure}.
This particular match has been detected as a "High Method Similarity" because many small subtrees in the AST of the upper method, have been found in the lower one, such as `String json = ...`, an `assertNotNull`, and the two `assertEquals` statements with a literal on the left and method call on the right. A large AST on the right elevates this as just a small set of subtrees relative to its size have to match. Reduction of these matches could be done by either increasing the required similarity for these matches, we have chosen 95% as it has shown good results elsewhere but this shows it can be approved further.

\begin{figure}[t]
    \lstinputlisting[language=java, label={code:jackson}, caption=Good fit of High Similarity measure]{code/jackson.java}
\end{figure}

We show in \autoref{code:jackson} an exemplary comparison, where the report contains a better match, as both methods generally perform a very similar function of reading a string to a class and extracting values. Though the extractability of common code is low, as both use different classes, the actual structure of the code is very similar and may be well maintainable, if grouped together.

The test structure of the \emph{jackson-databind} shows that some of the current techniques used in our test similarity measure, though methods like shown in \autoref{code:failure} produce faulty results and a better detection of similarity in these cases is required.

### Mockito

\begin{figure}[t]
    \lstinputlisting[language=java, label={code:mockito}, caption=Large Duplicated Method - Changes Branch]{code/mockito.java}
\end{figure}

In \autoref{code:mockito} we show a method, which has been changed to accommodate an API change which has taken effect, though with the introduction, we see that base different methods in the \emph{Base Branch} are almost identical to it, see \autoref{code:mockito2}.
In both methods, we find a large overlap of common sequence interspersed by some differentiating calls in between, due to the high relatively high complexity when compared with other tests and equal literals in almost all calls, an abstraction could be beneficial here as errors can be easily introduced.
Therefore, this report has shown a good adaptability to finding greater overlaps in this test set.

Though the "High Similarity Methods" measure shown here is not the only part of the report, common sequences can be found also, as seen in \autoref{code:mockito3}. 
They are superseded by the higher similarity of the test, but this is not especially excluded in the report, as both warnings can be useful to the end user.
Maintainers would need to check the generated warnings if situations as these occur.
As part of future work this behavior can be implemented to prevent manual intervention.

\lstinputlisting[language=java, label={code:mockito2}, caption=Large Duplicated Method - Base Branch]{code/mockito2.java}

A few more duplicated sequence are detected in the added and changed tests. To the greatest part, these are sequence of already existing builder sequence, preparing some objects to be used in a test.
If these sequences exceed a certain length set by the developers they could be extracted into a common helper method.

\lstinputlisting[language=java, label={code:mockito3}, caption=Short duplicated sequence]{code/mockito3.java}

Lastly, \autoref{code:mockito4} displays a match, which, while technically including the content of the new method, is misleading, as the base method is an actual test case of the code base, by coincidence containing the content of the newly added method and the new method does not perform a test on its own and is instead used as part of an object instantiated for other tests.

\begin{figure}[t]
\lstinputlisting[language=java, label={code:mockito4}, caption=Enclosed duplication]{code/mockito4.java}
\end{figure}

In general the technique has shown to work well on the recent commits of \emph{Mockito}, with a low rate of falsely identified changes.

### Junit5

As in the other repositories, similar sequence in \emph{Junit5} can be found in usage of object builder or initialization, an example for that can be seen in \autoref{code:junit}.
Analogous to previous examples of it, extraction may be done, but these short sequence extractions could lead to a reduction in code readability, therefore it is more sensible to keep them as is.

\lstinputlisting[language=java, label={code:junit}, caption=Duplicated Sequence]{code/junit.java}

One characteristic, only occurring in \emph{Junit5}, is the existence of exactly equal test cases under different names by design. For example, in \autoref{code:junit2} two methods with exactly the same content can be found, these tests call a method with a shared objects from the global scope, the actual test is if all methods are called correctly and in order under the test criteria. This is done via side-effects in the called methods.
While the detection of the code clones is correct, the implication of the warning is wrong as a removal or unification of it would modify the outcome of the tests. To avoid these false matches here exceptions to the "High Similarity Method" detection would be required scanning for such occurrences, though a general detection for most cases may prove hard to implement.

Next to these side-effects based tests, some tests in \emph{Junit5} exists that are empty (testing if tests can be named in certain ways). These, while not included in the set of commits chosen in this analysis, would encounter a similar issue. It would be possible to add configuration to the tool or add annotations to a method to prevent giving these false warnings in all situations.

\begin{figure}[t]
    \lstinputlisting[language=java, label={code:junit2}, caption=High Similarity Duplication]{code/junit2.java}
\end{figure}

\autoref{code:junit3} contains in the upper method, from the \emph{Changes Branch}, a duplication of the lower method, found in the \emph{Base Branch}. Both methods are almost identical except for the literals passed to the builder functions and check. An easy abstraction can be built by simply creating a new methods loading a passed csv file name and compare the size of the constructed stream to a given value.

\lstinputlisting[language=java, label={code:junit3}, caption=High Similarity Close Match]{code/junit3.java}

In our analysis of \emph{Junit5} commits, we noticed that one commit did not manage to pass the parsing and detection of commonalities of trees, though we did not find remarkable attributes about this commit some content exceeded the memory reserved for the analysis. With further testing this problem can get isolated and treated, but this is out of scope of this thesis.

Generally, \emph{Junit5} both contains changes which we correctly identify and changes in which our tool in the context of the code base incorrectly suggests changes. This shows that modifications or further restrictions of when to emit warnings is necessary, but the tool correctly identifies changes when they are present.

### Javaparser

The selected pull requests, contained in the \emph{Javaparser} project, behave similarly to the commits of the already observed repositories.
Most matches are concerning similar sequences, found in several positions of the code.
For example builder sequence like in \autoref{code:javaparser} are detected often, for which similar argumentation on the use of extracting these short sequences can be made, as we done in previous projects.
Also, one false match has been made due to the new method in \emph{Changes Branch} containing a low number of lines and the matched method in the \emph{Base Branch} having a much higher amount of lines.

\lstinputlisting[language=java, label={code:javaparser}, caption=Builder Sequence]{code/javaparser.java}

### Guava

Finally the \emph{Guava} project, the results analyzing this repository share commonalities with \emph{Junit5} and \emph{Mockito}.
Some empty methods are used throughout the code base, they are reported as having a similar content as other empty methods, for the same reasons as in \emph{Junit5} this warning is technically false and would in a pull request review cause more confusion than clarity about similarity in the tests.

Similar to \emph{Jackson-Databind} we can find multiple implementations of methods on objects which are essentially the same, an example can be seen in \autoref{code:guava} on which a wrapper around a method \emph{hashCode} to some identifier is duplicated. Depending on the situation this may or may not be replacable.

\lstinputlisting[language=java, label={code:guava}, caption=Common overwritten method]{code/guava.java}

In total the results on commits of \emph{Guava} are keeping within the expectation seen in other repositories, manual review has not found added test code with high similarity to already existing code, major misses of similar test case are unlikely.
