# Implementation

This section concerns the prototype implementation of our concept.
At the beginning, we discuss the environment we have chosen to implement our prototype in and the structure in which our project is constructed.
Following the basic procedures of the application are described with an overview of the used constructs in our abstractions.
Finally, we describe the command line interface we implemented for user interaction and the format of the output.

## Project Structure

\begin{figure}[t]
  \begin{center}
    \includegraphics[width=0.7\textwidth]{images/project.pdf}
  \end{center}
  \caption{Project structure in an overview diagram}
  \label{figure:project}
\end{figure}

We split our implementation into two parts, first the implementation of techniques described in \autoref{ast-based-test-similarity} named \emph{pentagrid}, and second the actual application \emph{ignitron} combining functionalities of this library and the context of the git based forking workflow for test code.
This structure is visualized in \autoref{figure:project}, included there are the main contributions of each part.
This split was taken to allow further use of the base algorithms of this project.
Both can be found free and publicly available. \footnote{\url{https://codeberg.org/jwuensche/pentagrid}}\textsuperscript{,}\footnote{\url{https://codeberg.org/jwuensche/ignitron}}

They are implemented in kotlin with gradle as their build system.
Kotlin was chosen due to its interoperability with java, which allows us to use widely used java libraries to parse and process the \gls{AST}.
We have chosen \emph{javaparser} \footnote{\url{http://javaparser.org/}} for this job, as it is a robust library which allows for uncomplicated java source code parsing, and the extensive usage of the visitor patterns allows for easy manipulation and extraction of subtrees, which we are interested in.

## Pentagrid Details

\emph{Pentagrid} deals with many internal operations we have described in the \autoref{ast-based-test-similarity}.
To parse, we use the aforementioned \emph{javaparser} to generate the \glspl{AST} of each file and extract required methods. On each tree we find a number of method definitions, the handling and abstraction of these is the domain of \emph{pentagrid}.

### Read-In

\emph{Pentagrid} supports reading of any Java source files, but due to the topic of research only cover the parsing of test source code here.
The location of test code is defined in the standard Java project structure to be located under \emph{src/test/java},\footnote{\url{http://maven.apache.org/guides/introduction/introduction-to-the-standard-directory-layout.html}} by this we can directly find source code which is relevant for us.
For projects which do not respect these structures, we use the next repository under the name `test` and get all `.java` files from it.
This already excludes possible resources and source code of other languages present in the project, for example, Kotlin preventing failure of parsing.
Once the files have been scanned we use the resulting trees representing each file and retrieve the mentioned method definition subtrees.
We restrict this to non-abstract as abstract methods do not carry an implementation, rendering them irrelevant for the purpose of our analysis.

These methods are then enclosed into a \emph{MethodBlock} which holds the tree itself and the accompanying metadata such as the \emph{CompilationUnit} it was registered in, the fully qualified name, and the annotations of the method, which we use to identify the location and the reference to the identity of the found tree respectively.

### MethodBlock

The \emph{MethodBlock} is primarily created to provided a wrapper for operations we want to perform on the whole tree of a method, this can be, for example, a translation to generalize the tree for future matching or to generate a hash value list.
Already implemented operations are, several translations to find more similar code paths, including the removal of comments, the generalization of variable names, and the generalization of literals.
Additionally, the matching algorithms mentioned in the \autoref{ast-based-test-similarity} are implemented and can be used on the \emph{MethodBlock} in combination with other instances. This returns a result containing a list of maximum isomorphic subtrees found in the two trees.

### Sequence Detection

The result of two methods can well be used to detect not only singular subtrees occurring in both, but if we interpret them further we can find consecutive expressions forming sequences.

\begin{figure}[htb]
  \lstinputlisting[language=java, label={code:sequence}, caption=Excerpt of Junit5 test code]{code/sequence.java}
\end{figure}

We explain how we do this in detail on a an excerpt of code from the \emph{Junit5} test code base, seen in \autoref{code:sequence}.
Displayed are two methods, both of which share a common sequence of code with each other, in the former lines 4 to 6 and in the latter lines 14 to 16.
Both methods are represented by a \emph{MethodBlock} in our representation, each of which holds a \emph{BlockStmt} including the content of them and their metadata.

When we match these two \emph{MethodBlocks} a \emph{Partial Equality} is detected, meaning that an overlap has been detected.
On further analysis of the returned result, we can see that the top most matches are the \emph{ExpressionStmts} residing on the lines previously mentioned.
For now we only know that they are common subtrees in both methods, to ensure that they are truly a sequence we now check which sequences each of these methods contains.

A sequence in an \gls{AST} can be defined as a set of nodes which fulfill three conditions, first they have to belong to the same parent, this requirement prevents completely foreign nodes to be detected as a sequence, as a sequence always inhabit the same level of one code block.
Second, these nodes have to be directly adjacent child nodes of said parent block to at least one other node in the sequence, this prevents lines from exclusion and sequence to be non-consecutive.
Third, each of these nodes has to be a subclass of \emph{Statement}, a \emph{Statement} describes in general a section of code which executes some function of the program like a for loop, an assertion, or an expression.

To perform this check, we can iteratively create a list of possible sequences and append each valid candidate from the following elements to a sequence, for each new node we create a new list to find variations of sequences.
Since the list of statements is ordered in our implementation, we find every possible sequence and subsequence.
We can filter them again, to exclude sequences of node references that are contained in at least one other sequence.
We are required to use referential quality here, since structural equality checks the characteristics of the object, which can lead to falsely excluding equal sequences thinking they are subsequences.
The result is then a list of all sequences contained in this method. This can be efficiently applied to all matched results in a parallel manner using the Java \emph{ParallelStream} API, since they are independent from each other.
In general we try to parallelize all operations which are sensible to do so.

### Comparison of MethodBlocks

The comparison we implement is a relatively simple hash comparison of linear sequences of nodes, we may perform this due to the characteristics of parsing the \gls{AST}, which are the nodes are in order and can be traversed that way.
This \emph{HashComparison} works on the principle of streamlining the tree into a quickly hashable stream of nodes.
For two given trees we can perform this hashing on each of its nodes and find an overlap of the stream of hashes with the hashed nodes of the other tree, if one can be found a subtree match has been identified.
For a result, we have to secure that they are the top-most matching subtree, as this greatly eases the usage of the found trees in the detection of sequences for example and the following possible replacement of sequences and blocks.
This can be again done on the stream of node hashes, of which we can check if the head of each is contained in another found subtree, if this is the case we discard this tree as it is already contained in the search in a higher level node.
The resulting \emph{HashResult} contains a reference to MethodBlocks in question in combination with pairs of equal node references to either tree.

## Ignitron Details

\emph{Ignitron} is the user application part of the project. It implements the command line interface, leveraging \emph{pentagrid} to parse the source code, finding and treating test similarity issues, and produces a human and machine interpretable output in JSON form.

### Structure

As already described in \autoref{ast-based-test-similarity} we have a pipeline like processing of the input, searching for relevant directories, beginning with the parsing of the test source followed by the filtering of found methods, to the generalization of these, the matching of these methods, and perform actions to reduce test similarity and warn if other similarities have been found which cannot be treated automatically yet.

### Parameters

\emph{Ignitron} accepts a set of input parameters both required and optional.
Only one parameter is required to be given, which is the pull request option pointing to a point in the git history which serves as the comparison point of changes to be added.
The rest of the parameters is optional and default to the following values.
First, the base branch to which the comparison should be performed, if no value is given this will always be the current branch. Notable is here that we always return to the active branch when the application was started not to the base-branch, by doing this we do not modify the repository in unexpected ways.
Second, an indicator, if a commit with proposed changes should be created, can be given. If no such indicator is given this falls back to resolving to false, again as to prevent unexpected modifications.
A third option is available, but simply renders a help page explaining the usage of the program.

### Git

As already mentioned we use the java implementation of git \emph{jgit} to interact with the repository. We set one restrictions on the usage of \emph{ignitron}, the current state has to be clean, as we cannot resolve eventual conflicts, which might occur otherwise, without the input of the developer, so we prohibit any action in this case.
This is already the case for continuous integration, the planned area of application, before reviewing a pull request.

For easier usage, we have created an abstraction on the branch by which we perform required accesses to the filesystem and the repository by that, for both pull requests and base branch one \emph{Branch} exists.
A \emph{Branch} object simply ensures that the git repository is currently tracking the correct HEAD, associated with this \emph{Branch} object.

### Method extraction

To reduce the amounts of methods, we have to analyze we reduce the method set to the minimal required amount.
As a first step, we have an overview of the changes introduced by the commits between the base branch and the pull request head, these will have happened in specific files, we analyze these files and use all which are referring to files in the test paths to find relevant files to be parsed.
This is only a valid constraint for the \emph{Changes Branch}, we still have to parse all of the source code in the \emph{Base Branch} to find possible clones, as we cannot exclude the chance that some code may have been taken from otherwise unrelated methods.

To further reduce the number of relevant methods, we check which methods in the given files have actually changed.
There are two approaches for this in general, we can either check the diffs generated by git and see which methods have been mentioned, or we compare the \glspl{AST} generated.
The interpretation of diffs requires additional effort, as we need to parse the diffs to identify the methods and files mentioned, this is a considerable initial effort for this validation, as relevant information, such as the signature, do not have to be included.
In contrast, the detection via an initial \gls{AST} comparison can be done relatively quickly and with functionalities already implemented.
To detect these changed methods with the help of \gls{AST} comparison, we perform a simple top-down comparison. If two methods with the same fully qualified name are different, if this is the case the method has changed. If no method with the same fully qualified name can be found in the changes, the methods has been moved or removed, which we mark to take into consideration later.

### Generalization

To find higher types of clones, we have implemented different methods of generalization of trees, we have described them briefly in \autoref{ast-based-test-similarity}. For different kinds of clones we combine different methods of tree generalization.
\emph{Ignitron} uses the \emph{CommentTransformer}, \emph{NameTransformer}, and \emph{LiteralsTransformer}, each of these change the tree content slightly to allow for a detection of almost similar subtrees.
We do not change the structure of the tree itself though this can be part of future work to allow structurally different but semantically equivalent.
We try to classify each found code clone to an according clone type, by doing this we can act accordingly for each code clone found based on their classification. To determine which classification they belong to we successively build up transformers relevant for each type of clone.

Type-1 clones can be found with the usage of the \emph{CommentTransformer}, since type-1 clones are only differentiated by formatting this set of transformers is sufficient. This is the case because further formatting differences have already been taken care of by the parsing stage and the creation of the \gls{AST}, other differences are not present at this stage of processing.

\begin{figure}[htb]
  \lstinputlisting[language=java, label={code:helper}, caption=Two test using helper methods]{code/helper.java}
\end{figure}

Type-2 clones are split into two categories in our implementation, on the one hand the abstraction of identifier names of variables with the \emph{NameTransformer}, and on the other unifying literals using the \emph{LiteralsTransformer} to find a similar code structure with slight alterations.
We made this split to better accommodate often occurring patterns in tests, which might already be part of an abstractions such as calling helper methods.
An example for this can be seen in \autoref{code:helper}, this snippet is taken from the \emph{Junit5} test suite and contains a low amount of helper methods being called in functions for a better readability for the test suite maintainers.
We find that on the generalization of literals the lines 3 to 4 and 15 to 16 could be seen as equal, but this may not be desired as this code snippet already works with a builder pattern used abstracting the process itself.
Adding another layer of abstraction, with a method receiving an argument to either call `delimiter` or `delimiterString` and returning the build object, or a method returning a partially constructed builder, would reduce the similarity of the test, but also obscure the readability and therefore harm maintainability.

When we apply the \emph{NameTransformer}, variable names are abstracted and enumerated based on their type and already abstracted variables, based on them we exclude this behavior and find more likely assignments of variable, which are equal on the right part.
These can potentially be abstracted into their own methods outright.

Methods found with the application of the \emph{LiteralTransformer}, due to the aforementioned reasons, may not be treated automatically, as to avoid over extracting out of test methods.

### Comparison

After we create all sets of method generalizations required for classification, we compare the \emph{Base Branch} and \emph{Changes Branch} variants to find possible similarities.

All tests of the two branches are compared to other tests treated with the same set of generalization transformations, as to not obscure the results and find clearly distinguishable type-1, type-2 without literals, and type-2 matches.

To perform the actual comparison in this situation, we implemented the hash-based comparison described in \autoref{ast-based-test-similarity}, by performing this comparison we find all maximum equal subtrees of the methods including all of their occurrences in the tree of their reference \emph{MethodBlock} and by that also of their \emph{CompilationUnit}, which represents the completely parsed file in its original state.

Though this comparison requires a higher computational effort than we need in other situations in our process. For example, the initial comparisons to find non-changed methods in our variants does not have to be as exhaustive as the hash comparison, thus a simpler top-down comparison is chosen. This more superficial comparison provides a faster termination, as no other information than a boolean is required, indicating the equality relative to the two roots.

Generally, we check here how to isolate trees as passing the reference of a node would inevitably result in unforeseen changes if one of them gets changed, therefore we treat \emph{MethodBlocks} as immutable and store a reference to the node, residing in the \emph{CompilationUnit}, for alteration of the source code.
Only the base \emph{CompilationUnit} should ever be modified.

### Actions

Once we have gathered necessary data about the similarities of test cases and their structure and repetition, we can take action by altering the \emph{CompilationUnit} according to our recommendations.
For this we consider first the type of node we have found the in common.
Depending on this we can suggest better alternatives for code structure.

First, equal \emph{BlockStmt} may be spotted on a few occasions, most common is that it represents the content of a method.
In this case, we check if their annotations or class, in which they are contained, are differing as not to delete test cases which environments are changed, possibly testing side effects on the contained code.
If this is not the case, we can safely remove the redundant method added in the \emph{Changes Branch}.
Other \emph{BlockStmt} which might be encountered inside of \emph{BlockStmts} of methods, have to be further analyzed to check which environment they need, for example, variable reference used. This is for example the case for anonymous objects or loops.
We have not implemented this because of the rarity this occurred in Java test code, but the topic can be included in future work.
The implementation would also benefit from resolved types, to better understand the context the block is used in.

Second, Sequences, which we have already detected previously, can be extracted similarly to \emph{BlockStmt}, though here we have to again analyze which variable reference have been used.
To guarantee working code after the changes have been applied, the former location of the sequence has to be replaced with a call to the newly created method containing the extracted sequence.
A warning if these are found is emitted currently.

Third, the overall similarity of two methods is calculated, by the amout of common trees we have found in both \emph{MethodBlocks}. This value is determined by the amount of nodes contained in the common trees compared to the method overall.
If more than 95\% of nodes of left value in the comparison are also contained in the common trees the left method is considered to be of high similarity to the right method.

### Output

The output is divided into two parts, changed and warnings, an overview of the specification can be seen in \autoref{code:format}.
Changed contains every transition we have done in the code to transform it to the current state, this is separated into actions which were the cause of the modification.

Following, Warnings contain code patterns which are noticed but may not be treated by the tool such as inner \emph{BlockStmts} or higher level code clones, this serves to pass information found which we could not further analyze due to the lack of additional information or it being out of scope. These warnings include sequences which are not extracted in the current implementation.
The warnings in general are a compromise as to not alter too aggressively and extract to the point of reducing maintainability and the complexity required in the detection of higher problems.

\begin{figure}[htb]
  \lstinputlisting[language=java, label={code:format}, caption=Format structure of the output]{code/format.json}
\end{figure}

Common attribute between them is the location of the code which is described as the path of the file in which the occurrence took place accompanied with the range of lines.
If a single file has more than one occurrence, it will be listed more than once in this list of locations.

The \emph{action} and \emph{changes} further describes what exactly has been done on the specified changes, for instance, the \emph{action} "extract" always carry in the changes value the extracted sequence enclosed in the new method created. For the \emph{deduplication} action the remaining block is given, as this part of the code has been deduplicated.

