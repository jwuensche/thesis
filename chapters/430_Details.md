## Preprocessing techniques

To find certain types of clones, we need to modify \glspl{AST} involved further, to fulfill certain criteria of types of clones.
Starting with type-1 clones which exclude whitespaces, formatting, and comments.
Later, type-2 clones include the additional omission of identifier names and literals.

One point which sets our technique apart from other classification, is that names and literals do not have to be generalized in conjunction, as some operations such as finding equal method content can be done with literals abstracted, but the change of literals can indicate that the another method could be an independent test case for example. As already discussed in the \nameref{internal-workflow}.

### Whitespaces, Comments and Formatting

Thanks to our basis on \glspl{AST}, we get easily get rid of whitespaces and formatting difference, as they are already handled by the parser. The once parsed \gls{AST} does not contain any of these differentiating characteristics anymore.
Still, after we receive the \gls{AST}, we have to modify them to detect type-1 clones, as comments are still part of the \gls{AST} representation.
To achieve this, we remove all contained comments, as they are not influencing the semantic structure of the program.
The cleaned trees are then passed on and to allow the detection of clones type-1.

### Identifiers

\begin{figure}[htb]
  \lstinputlisting[language=java, label={code:simpleTest1}, caption=Simple test in java]{code/simpleTest1.java}
\end{figure}

The two code snippets \autoref{code:simpleTest1} and \autoref{code:simpleTest2} serve as an example to demonstrate potential code snippets affected by identifier abstraction.
They are semantically equal, only differing in their chosen identifier, for methods and variables.
The test, `addTwice` (\autoref{code:simpleTest1}), contains adding two numbers into an instance of a `List`.

\begin{figure}[htb]
  \lstinputlisting[language=java, label={code:simpleTest2}, caption=Simple modified test in java]{code/simpleTest2.java}
\end{figure}

The second test, `listAppend` (\autoref{code:simpleTest2}), is equivalent with the exception of the identifier name given to the `List` instance.
In our tree we can see that we perform the same operations on both object, namely creating an instance and calling two methods on it.
But a direct comparison would fail since they are not equal nodes due to their differing identifier in an \gls{AST}.
For this equivalence to be found, the trees have to be generalized to a degree, eliminating the differentiating characteristics.
We are going to dismiss the method name for now as it is unimportant in our matching of test contents.
In \autoref{code:simpleTest1} we can see that the variable `foo` has the given type of `List<Int>` in our technique the name of this variable will be replaced with a string containing an enumeration and the type, for example, in combination this can be `List<Int>0` for the first encounter of this type.
During this replacement the original definition and all following occurrences are replaced with this new identifier.
After applying this scheme, `bar` in \autoref{code:simpleTest2} carries the same new identification features, so they can be detected as a type-2 clone.

A downside of replacing identifiers with this approach is that in some situations some equal statements may get lost. This is due to additional declarations of variable with the same type before hand, which are not used in further code. These situations are rare but would produce a faulty result here.
Since the problem of variable equivalence is undecidable \cite{alpern1988detecting}, no correct solution may be found here, but many approximating algorithms exist giving an best effort estimation, for example, in \citeauthor*{alpern1988detecting} \cite{alpern1988detecting}.
While others like \citeauthor*{baxter1998clone} proposed to ignore the occurrence of names completely relying on the structure of code. Those can be used instead of this rather simple scheme, but would increase the calculative effort required.

### Literals

To complete the detection of type-2 clones, we also need to extend the generalization to include any literal values used in the code.
This can be helpful in situations were long initialization code is used and has not been extracted to its own methods.
For ease of integration we may modify each appearance of literals to a basic value depending on its type, for example, a `String` value to an empty `String` or an `Integer` to a zero value.
In languages in which types are not available, they can be set to a single unified value like a numerical zero.

## Matching Algorithms

We use two different matching algorithms in our technique to determine equality or other possibilities as described in \autoref{equality-in-trees}. For a general decision if two trees are equal are equal, we use a simple top-down comparison with no extra information or generalization.
Its advantage is the fast termination in case trees are not equal on a high level, which since we perform a large amount of these comparisons scales better in size as most differentiate in the first children from the root node.

For a more detailed comparison, we have implemented a hash list based comparison which searches for equal subsequences of hashes in its counterpart's hashes.
This comparison results in a list of all possible subtrees contained in both trees.
To filter this list, we exclude all subtrees which are included in other subtrees we pick the node first in each found hash list and check if it occurs in any other list we have found, if this is not the case the current subtree is the topmost subtree which is equal.
The root of each found topmost isomorphic subtree can then be used to classify the matches found and perform operations like sequence detection on topmost statements for example.

## Equality in Trees

\begin{figure}[thb]
  \includegraphics[width=\textwidth]{images/equality.pdf}
  \caption[Equality in Trees]{Two example trees showing possible types of equality. Same colors indicate the nodes which are equal in both trees.}
  \label{figure:example_graph}
\end{figure}

Depending on the kind of match found, we can apply different tactics on how to treat them. We named situations in our technique in \autoref{internal-workflow}.
We explain here shortly the theoretic possible cases we can find in tree matches and how they relate to those situations.
Since we have no knowledge about how two trees are constructed compare to one another, we have four basic cases which can occur in relation to the root node of each tree.

\begin{description}
  \item[Full Equality]
implies that both trees perform the exact same operations and can be merged without many modifications. Good candidates for this can be simply duplicated tests under different names which occur multiple times. These are less likely to happen but can occur nonetheless.
  
  \item[Partial Equality]
describes two trees which share common subtrees. An example can be seen in \autoref{code:partialeq}.
A positive example when this can be effective is boiler-plate code, the instantiated objects can be then passed back as a result of it.
We show in \autoref{figure:example_graph} an example, in which both roots, of the left and right trees, share a subset of nodes.

  \item[Partial Full Equality]
describes one tree that is completely enclosed in the other tree. In this case the inferior tree can be removed. This can be seen in \autoref{figure:example_graph} where the outer left child of the left tree shares a sub-tree which is equal to the root of the right tree.

  \item[Unequal]
trees do not share sub-trees to a considerable amount and can be safely dismissed, as these are probably newer content or modification of higher semantic difference. Seen in \autoref{figure:example_graph}, where the outer right child of the left tree does not share any commonalities with the root of the right tree.
\end{description}

All of these are represented in different areas of our technique.
\emph{Partial Full Equality} and \emph{Full Equality} always represent an equality on the level of the block statement of a method. 
In a \emph{Full Equality} setting, these are then equivalent, in a \emph{Partial Full Equality} one of the methods content is entirely included in the other an example for that can be seen in \autoref{code:partialeq}.

\begin{figure}[htb]
  \lstinputlisting[language=java, label={code:partialeq}, caption=A collection of tests]{code/partialeq.java}
\end{figure}

For \emph{Partial Equality} some common subtrees are found, but cannot be classified further, as they do not carry a relation to any root of the trees.
And lastly, if two trees are deemed \emph{Unequal} we can safely dismiss this match and continue.
