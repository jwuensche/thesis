## Static Analysis

Static analysis describes the inspection of code without the execution of it, allowing for a predictable execution time and low overhead for the setup of the analysis itself.

There are several methods proposed and done to implement a specific static analysis, common ones are abstract syntax tree based, textual interpretation, token based, or via graph analysis of dependencies in source code.
All carry some advantages to each other.
We have decided that an abstract syntax tree is fitted best to our needs, which we discuss in \autoref{related-work}.

Static analysis is a widely used approach to understanding source code, it finds application in compilers (e.g. GNU compiler collection,\footnote{\url{https://gcc.gnu.org/}}clang,\footnote{\url{http://clang.llvm.org/}}, or rustc\footnote{\url{https://github.com/rust-lang/rust}}) in code linters to improve code quality (e.g. rust-clippy, \footnote{\url{https://github.com/rust-lang/rust-clippy}}clang-tidy,\footnote{\url{http://clang.llvm.org/extra/clang-tidy/}}, or SonarLint\footnote{\url{https://www.sonarlint.org/}}), vulnerability detection for well known security exploits \cite{chess2004static, yamaguchi2012generalized}, refactoring of code \cite{feldthaus2013semi, kataoka2001automated}, and the detection of code clones and code similarity, for example, to improve code maintainability (e.g. CloneDR \cite{baxter1998clone}, Deckard \cite{jiang2007deckard}, NiCAD \cite{cordy2011nicad}, and SimScan \cite{duala2007tracking}).
Disadvantages of static analysis can be found in highly complex problems in which the automated detection may not be achieved efficiently, since the cost of implementing a technique to detect them statically is too high \cite{anderson2008use}.
Other means may provide a more straight-forward implementation, for example, in memory management observations.

Focus of current research lies on the usage of static analysis to identify potential bugs, bad code practices and security issues.
An example for this is a recent paper about the analysis of possible speculative execution, triggering unclosed spectre bugs in C code.
\citeauthor*{oleksenko2020specfuzz} use a combination of static analysis and fuzzing to check for patterns which could potentially lead to eagerly executed branches \cite{oleksenko2020specfuzz}.

We have chosen to base our technique fully on static analysis, as our consideration lie upon efficiently analyzing software systems, without the need to build and execute them, for profiling or call stack tracing.
Requiring both building and execution, followed by the analysis of results, increases the execution time required for the process to complete greatly.
Static analysis serves a good fit in this domain, as we are only required to parse the given source code, given it is valid, and receive the \gls{AST} generated for further analysis as an useful abstraction.
Therefore, relying solely on static analysis allows our computation time to be predictable to the amount of input we receive.
Furthermore, we are independent from the build systems or presence of system dependencies, making our approach flexible in application.

### Test Generation via Static Analysis
  
Test generation relying purely on static analysis has been exhibited to generally be relatively complex and perform worse in coverage than dynamic analysis approaches \cite{zhang2011combined, godefroid2007compositional}.
Therefore, it is most often used in conjunction with other techniques, such as dynamic analysis, restricting the often over eager test cases by restricting the tests to certain constraints \cite{zhang2011combined, godefroid2007compositional, chebaro2010combining}.
There have been multiple implementations, showing that this combination of techniques works acceptably, namely here is \citeauthor*{godefroid2007compositional} \cite{godefroid2007compositional}, they improved their previous dynamic approach by static analysis.

### Static Analysis on Tests

Most of the presented examples are not specifically restricted to any one kind of source code purpose, they can be generally applied to find certain characteristics in any kind of environment.
Though there are tools which specify in certain areas of code, for example, the generation of test cases \cite{noy2004system}, or the inspection of test code to better understand bug origins \cite{sreedhar2007system}.
More recently \citeauthor*{cartaxo2011use} explored the reduction of test cases via a similarity function, as they experienced a large duplication and overlap of generated test case. They use a path based approach in which all possible paths are analyzed in a matrix.
These matrices are compared to each other, matrices over a certain similarity value are then treated as similar test cases \cite{cartaxo2011use}.

### Alternative to Static Analysis

Code analysis does not have to rely on static analysis. Dynamic analysis for example works on the tracing and profiling of a running program.
This has certain advantages when compared to static analysis, for one more complex metrics can be recorded such as code coverage, commonly used applications are code coverage test suites (e.g. coveralls \footnote{\url{https://coveralls.io/}}) which track which lines are visited at least once in the test suite, more sophisticated tools may also provide heat-maps showing most often used paths of execution, or heap usage detection, for instance, valgrind \footnote{\url{https://valgrind.org/}} a tool to test programs for memory management issues.
But also more complex issues can be investigated with a dynamic analysis approach, such as the detection of errors in parallel programming, for example, as done by Intels VTune. \footnote{\url{https://software.intel.com/content/www/us/en/develop/tools/vtune-profiler.html}}

The major disadvantage present in dynamic analysis, are the discarding of code quality in the analysis and the requirement to execute the program in question, which can be resource intensive for larger systems.
Additionally, the program has to be able to run on the platform in use, which can require the use of virtual machines if the target architecture is different from the hosts. Further, the initial compilation is required, which can take, for larger software systems like the linux kernel, chromium, firefox etc., several minutes to hours before compilation is completed, depending of the hardware capabilities of the host, therefore restricting usage because of the effort implied analyzing code.
