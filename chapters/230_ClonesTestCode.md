## Clones in Test Code

Recent research has indicated that the number of clones is higher in testing code than in production code \cite{bladel2020clone}.
This is to be expected, as tests contain often occurring patterns, which can be duplicated and slightly change for different test scenarios.
The changes of one code pattern can lead to test smells as all tests, which contain this common code pattern, have to be modified in a similar kind, otherwise test behavior may be changed unexpectedly as the old code might still compile but behave different than intended, for example, when default values are used in error cases.
\citeauthor*{bladel2020clone} claim they found 23-29% duplication in test code, in comparison to 7-23% in production code.
The prevalence of test clones over production clones has also been found by \citeauthor*{tsantalis2015}, in their extensive study they compared different tools such as NiCAD \cite{cordy2011nicad}, CloneDR \cite{baxter1998clone}, Deckard \cite{jiang2007deckard}, and CCFinder \cite{kamiya2002ccfinder} on tests.
Furthermore, they found that the refactorability of production code tends to be greater than in test code \cite{tsantalis2015}.
  
