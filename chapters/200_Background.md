# Background 

In this chapter we explain at first the basics of code clones, including a short explanation how they are created and in which manner these impact the development process, followed by the basic concepts of \gls{AST} and Static Analysis and how presented techniques can be utilized to analyze source code for test code changes.

