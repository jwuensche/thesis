## Discussion

In our analysis we find that the approach produces good results in most situations, but requires more adjustment to produce more meaningful output, as some situations create matches on empty methods or side-effect driven tests, which in itself are correct matches but do not serve the purpose of detecting code duplication and helping maintainers, as they are purposefully designed and created to behave in this manner.
Additionally, in case of short methods (one to five SLOC) false matches occurred if other methods in comparison were greatly larger, as seen in the commits belonging to the \emph{Jackson-Databind} project.

Generally, we did not miss major duplication in the changes of selected commits, more often the matching was too inclusive, as in described error cases.
This indicates that future focus should concentrate of better understanding the context of in which tests are used, to be able to give more automated suggestions and better create warnings.

One technique implemented by us has not stepped into effect, the unification of exactly equal blocks has not been observed as the restrictions, explained in \autoref{implementation}, were rather strict compared with the others, though this stands in relation to our created dataset, which only contains mature projects and recent commits of theirs.
This maturity in the project comes with a routinely review process of code changes before they are merged.

## Performance

The complete dataset was analyzed in 96 minutes and 18 seconds on the evaluation server.
This results in an individual execution time, for each pull request, of about 1 minute and 55 seconds, fitting well into the planned workflow of execution on demand in the process of pull request reviews.
A greater reduction in time can be achieved by tweaking the comparison algorithm to be more time efficient. This would open up the possibility of extending the use to more situations, such as during the development process, if this is desired.

Performance limitations may be found in the memory footprint of the prototype implementation.
Three of our queries ran into memory problems as the Java virtual machine ran, even with the allotted maximum memory size, into an out of memory error.
Further analyzing this issue led to reduction of behavior, but we were not able in time of finishing to avoid it entirely. With further investment of work, this issue of memory overuse, could be eliminated entirely.

## Technical Limitations

The issue of automatically reducing the amount of similarities in methods can be done, but is part of future work.
This analysis shows that the issue is complex in its execution and edge cases are common. Enhancing \emph{ignitron's} ability to carry out these simplifications can be done as part of future work.

## Threats to Validity

The general validity of this evaluation can be dispute on the following aspects, through further studies these may be researched to explore more characteristics of the proposed technique.
First to name, the dataset we created is relatively small as it had to be evaluated by hand. Only 50 recent pull requests of five repositories had been assessed, more repositories and older pull requests can be added. Historic data in form of older pull requests can offer valuable insights on how the technique performs in a developing system.
Second, the project range selected is narrow as we chose to pick popular Java repositories. Less popular Java repositories might perform differently due to the state of their maintenance or similar factors.
Third, the acuity of results can be improved by including other testing code such as found in \emph{C}, \emph{Rust}, or \emph{Kotlin} projects.

 
