## Abstract Syntax Tree

An \gls{AST} is a hierarchical representation of a given source code's syntactic structure \cite[41]{dragonbook} \cite{roy2009comparison}.
A number of implementations aim to understand and analyze source code using \glspl{AST}, for example, the exploration of change in software \cite{neamtiu2005understanding}, the detection of code clones \cite{baxter1998clone, jiang2007deckard, sager2006detecting}, or the detection of vulnerabilities \cite{yamaguchi2012generalized}.
Though they are more commonly known as an intermediate representation in compiler architecture, acting as a well processable data structure between the purely text source code and the target of the compiler, such as machine code.

The choice of trees to represent source code also allows to build the approach of finding similarity on already proven algorithms.
Tree comparisons is a topic of research since the early days of computer science, and has been discussed in numerous papers, trying to optimize the comparison and search in trees.
We are particularly interested, in the detection of patterns or common subtrees in \glspl{AST}, together with the generalization of trees.
Many algorithms have been proposed to find common trees in forests \cite{nijssen2003efficient}, or tree pattern matching algorithms reducing complexity \cite{valiente2000simple, hoffmann1982pattern, cole1997tree}.

### Applied Matching Algorithms

A number of algorithms have already been used to identify similar code in various code clone detection techniques. We name here a few selected unique or renowned techniques, but more can be found.

First to mention is \citeauthor*{dragonbook} \cite[358-362]{dragonbook}, they comprised a method to construct Directed Acyclic Graphs to identify common subexpressions in a syntax tree.
In their technique, they propose a generation of a set of nodes, which are equivalent state transitions to the \gls{AST}.
Furthermore, they build with this set of transitions a Directed Acyclic Graph, which has the common subtrees as unified nodes.
This approach works well enough to find common expressions in a single tree, and can be expanded on working on multiple trees for foreign equal expressions.

\citeauthor*{baxter1998clone} \cite{baxter1998clone} then expanded on this idea for the development of CloneDR, in which they applied this base algorithm to find approximately similar trees and order them into buckets, to then further compare them to other trees in the same bucket finding exactly equal and similar trees in a scalable manner.
This extension is explicitly useful in the detection of code clones, these near misses can be extracted to find refactorable code snippets if some further abstraction is applied.
The advantage of this technique lies in the reduced cost for comparisons, since no all-to-all matching of subtrees has to be performed to find commonalities, reducing the complexity of it.

There are more ways to find common subtrees efficiently, one would be the alteration of naive tree pattern matching.
This idea of tree matching stems from the base idea of giving a tree as a base pattern and checking if another tree can fulfill it.
The naive implementation of tree pattern matching would result in a complexity of $O(nm)$, with $n$ being the amount of nodes in the to matched tree and $m$ the size of the pattern \cite{valiente2000simple}.
An interesting technique has been proposed by \citeauthor*{valiente2000simple} \cite{valiente2000simple}, they use the work of other already existing algorithms of finding common subexpressions by \citeauthor*{downey1980variations} \cite{downey1980variations} and \citeauthor*{flajolet1990analytic} \cite{flajolet1990analytic} reduced the problem of finding subtree patterns in a forest onto it, essentially being able to find all subtrees of the forest isomorphic to all possible subtrees of the pattern tree.
This allows to find common subexpressions and even smaller granularity subtree matches, while reducing the amount of matches total needed.
The algorithm proposed performs a bottom-up comparison, which leads to the downside that we may not find greater similar subtrees, because of differentiating nodes on a path in the tree.

A different approach has been done by \citeauthor*{neamtiu2005understanding} \cite{neamtiu2005understanding}, they created a mapping and matching algorithm for two versions of the same \gls{AST} and used this mapping to explain the historic changes of the underlying source code.
To create the mapping they used a parallel iteration over nodes of both \glspl{AST} and update the found node difference as long as node mismatch happens \cite{neamtiu2005understanding}.
The disadvantage of this top down traversal is that once a mismatch occurs, the traversal stops and all nodes below it will get dismissed, leading to information loss, generally it can be used to match a rough overview of the trees due to these detail lost.
Similarly, to the behavior as observed in \cite{valiente2000simple}.

This problematic is shared in general with other matching working on a top-down approach, similar being the Top-Down-Maximum-Subtree-Isomorphism from \citeauthor*{sager2006detecting} \cite{sager2006detecting}, they encounter the same problems of cut-offs in these situations.
In real case scenarios this can happen but depending on the situation this may have worse or neutral outcomes, for example, \citeauthor*{sager2006detecting} \cite{sager2006detecting} have found that in their detection of similar Java classes the top-down approach worked better than the modified bottom-up (modified from Valiente \cite{valiente2000simple}).

### Tree generalization

Part of the detection of higher level types of clones is to recognize not only completely identical subtrees but also partially identical trees which might still hint to the structural closeness of the observed trees.

\citeauthor*{yang2019function} \cite{yang2019function} presented a way too achieve this, by modifying the structure of observed trees.
They explored the possibility of turning alternative syntax of statements into a more generalized form and utilize this to detect potential type-3 clones in Java source code.
Mainly focusing on Java's downward compatible features such as lambdas and anonymous classes, additionally they try to reduce the variety of type definitions introduced with the introduction of generic inference.

Other ways too achieve this are included in the already mentioned hashing phase of CloneDR by \citeauthor*{baxter1998clone} \cite{baxter1998clone}, they choose a purposefully bad hash and take a modulo value of it to match closely related trees. In their paper they mention, that they omit leaves as small subtrees seem to skew the results the most, when searching for nearly similar trees.

Lastly, \citeauthor*{chase1987improvement} \cite{chase1987improvement} has proposed an algorithm, to match patterns including wildcards in patterns, allowing for a generalization in the context of the algorithm.
This way, one can specifically exclude nodes, which are not checked by the comparison, possibly allowing near-miss matches.
