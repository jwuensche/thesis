## Code Clones

Clones can generally be described as copied source code in different variations. In our work we use the definitions of \citeauthor*{roy2009comparison} \cite{roy2009comparison}:

\begin{description}
\item[type-1 clones]
carry minor changes such as whitespaces, comments, and empty lines added or removed. They are generally easier to identify as the actual symbols used are the same.
\item[type-2 clones]
include the characteristics from type-1 clones but also change identifiers like variable, interface or class names. Additionally constant literals may differ.
\item[type-3 clones]
incorporate the former, while allowing the removal and addition of statements.
\item[type-4 clones]
in comparison are only semantically equivalent code clones, meaning one clone gives the same result as another, but the way this is achieved can differ drastically. We omit these kind of clones in our observation as they are generally challenging to detect and certainty in the result is not possible due to the underlying problem of program undecidability \cite{luckham1964undecidability}.
\end{description}

One deciding factor why code clones have been subject of research is their impact on code quality.
Early papers have shown code clones degrade code quality and by that increasing maintenance cost \cite{baker1995finding}.
To reduce these factors, multiple papers have formulated an approach to find these clones reliably.
The most prominent examples are NiCAD \cite{cordy2011nicad}, CCFinder \cite{kamiya2002ccfinder}, CPMiner \cite{li2006cp}, Deckard \cite{jiang2007deckard}, and cpdetector \cite{koschke2006clone}.
All of these aim to find existing code clones so that developers are aware of the state of their code and possible impacts on code quality.

Notably, code clones themselves are not necessarily indicators of bad code. For example, the overhead introduced by a method call may not be worth the improved structural clarity of the source code \cite{baker1995finding}.
Depending on the runtime environment the program is intended for this has an overall greater or lesser impact on total performance, for instance, while embedded applications may aim to minimize function call overhead, compromises such as additional method calls are regularly done in programming languages like Java to structure source code in an object oriented manner.
There are more reasons, which explain the existence of code clones, for example, copying code often happens as developers need an already implemented functionality without restructuring essential parts of the code, since this would take up more time.
Additionally, developers may use some similar behavior for common tasks, for example, to access data, or make sure values are valid etc., which can lead to unintentional clones.
This can also happen in the use of already extracted code clones, for example, helper methods which are called in succession.
Extracting these again may lead to code which does contain less clones, but the clarity of the code may be reduced due to nesting calls for simple but often occurring operations.

### Program Equivalence

An often cited early source for clone detection is \citeauthor{baker1995finding} \cite{baker1995finding}. They use in their tool dup a technique based on textual interpretation and generalization via parametrization to find code clones in C source code, exemplary done on the X Window System.
They demonstrated an optimistic estimation that the code size could be reduced by up to 12\% when code clones are completely eliminated.
But further building on this approach was not likely to succeed as found clones do not correspond to semantic units of the source code.

Additionally, this work raises concerns about the impact code clones can have on the further development process, as dead patches can occur when cloned code is updated in only one location, leaving other vulnerable to the same bug if only partially removed \cite{baker1995finding}.
Possibly detected clones in this study are of type-1 and type-2 as they do not differ in structural form.
The approach has not been extended to detected syntactically more different but semantically equal clones. 
The finding of these may be harder in this technique since the scanned segments (lines of code) are not representative of the expressions the source code semantically conveys.

\citeauthor*{baxter1998clone} \cite{baxter1998clone} initiated the work on CloneDR, they propose to use ASTs to find clones and near-miss clones on C code bases.
The work is using \citeauthor*{dragonbook} \cite{dragonbook} to apply the compiler method of eliminating common sub-sequences to find code clones, as they are essentially common sub-sequences.
Their work has shown that this approach works well for the inspected source code repositories and offers good scalability without sacrificing accuracy.
In tests they reported the rate of clones matched their expectations of around 7-15% if the total code base each, though they do not specify the actual project used for this.

Another topic tied with the search of code clones is the detection of equivalence in variables, \citeauthor*{alpern1988detecting} \cite{alpern1988detecting} developed an algorithm to determine if two variables are equal.
The algorithm is designed to capture a large subset of the in general undecidable problem of program equivalence, to achieve this goal they use control flow graph analysis to find points in the execution of programs where variables can be considered equal \cite{alpern1988detecting}.
Multiple points in their research can be used to refine an approach to detect code clones, for example, verifying the equivalence of variables used in two code clones.

Further, \citeauthor*{wuu1991identifying} \cite{wuu1991identifying} used the linear space algorithm for computing maximal commmon subsequences proposed by \citeauthor*{hirschberg1975linear} \cite{wuu1991identifying, hirschberg1975linear}.
The goal of this paper was the detection of similar code which is differently structured to improve on output such as the UNIX diff tool.
Internally this works by assigning a weight to nodes in the tree and calculate their parents weights based on them.
Nodes with a certain set weight are then treated as similar.
