## Internal Workflow

To give a better insight on how this process works, we explain here the details for each step defined in our process overview.
First, we describe the internal working, which is equivalent to the process as shown in \autoref{figure:overview}. Afterwards, the \nameref{external-workflow} the is given in detail, as envisioned by us in combination with a \emph{Forking Workflow}, though alternatives are named. The \nameref{external-workflow} describes, how the process is seen by the external user of a potential tool.

### Prerequisites
The repository in question contains at least two branches, the \emph{Base Branch} represents the most recent state of the repository, the \emph{Changes Branch} persists of changes which are planned to be applied on the \emph{Base Branch}.
For example, in a pull request exactly this situation exists, as the \emph{Base Branch} is the target on which the \emph{Changs Branch} should be applied on. Therefore, the \emph{Changes Branch} is the tagged remote branch object in this pull request.
 
### Pre-Filtering
After the prerequisites are met, we can proceed to identify files, which are relevant for our analysis.
To reduce the amount of files that have to be parsed, we can create some restrictions, as to which should be taken into consideration.
For the \emph{Changes Branch}, due to the repository being git based, we can extract all changes as \emph{git diff}, which mentions specific lines of changes and the files they occurred in.
As the \emph{Changes Branch} is otherwise identically, we can dismiss all files not contained in the \emph{diffs} of commits between the branches.
Furthermore, as we are interested only in test code, we can exclude all code not included in the test source code directories. In Java this is by standard a directory with the name `test` under the root `src` directory. \footnote{\url{http://maven.apache.org/guides/introduction/introduction-to-the-standard-directory-layout.html}}
For the \emph{Base Branch} we cannot pick exact files, as we do not know which parts are relevant, therefore we parse all test source code files contained in source code directories.
Though multiple of these may exist in a repository hosting multiple sub projects, to counter over eagerly parsing all available test code in total, we can constrain test directories, which contain at least one test source file affected by the pull request.

### Parsing of Files
With the completion of selecting relevant paths, we can continue to parse them to retrieve the relevant methods.
For each contained method we retrieve the fully qualified name, the content of the method itself, all attached annotations, and the file in which it is declared.
An inconvenience arises in the \emph{Changes Branch} as the parsed methods even if they were in the changed files, do not have to have changes compared
to the \emph{Base Branch}.
We can sanitize them by checking the version of the method in the \emph{Base Branch} and \emph{Changes Branch} based on their absolute class path and comparing them naively with a top-down approach. This comparison is a good fit as differences are detected early and disrupt the comparison progress.
If the comparison results in both being equal we can safely discard these methods.
An alternative way to implement this is the closer inspection of the branch diffs, problematic is in this case the interpretation of the textual diff into which methods have actually changed.

Regardless of the way implemented, the result of this stage is a collection of methods for each branch only containing methods actually changed in the diffs.

### Filter Comparisons

Regarding the scope of this thesis a clear definition of comparisons required is given. Methods in the \emph{Changes Branch} are compared to already existing methods in the \emph{Base Branch}, this allows finding common sequences and blocks eventually duplicated or covered by other test scenarios.
Following this, the amount of required pure tree matches is the number of relevant methods in the \emph{Changes Branch} times the number of relevant methods in the \emph{Base Branch}.

A further reduction of complexity could be done by lending the technique of preliminary hashing of nodes into buckets which are then compared further \cite{baxter1998clone}.
Including this allows a more performance oriented implementation for larger diffs, which are more rare than in general clone detection as in \citeauthor*{baxter1998clone} \cite{baxter1998clone} scenario, but will speed up the process due still.

### Hash Comparison
We have chosen a hash based comparison algorithm for comparisons classified as relevant.
This algorithm is based on pre-hashing all nodes of a given tree and create for a comparison an intersection of its list of hashes with the list of hashes for another tree.
This yields all possible common subtrees contained in both trees, in our implementation we receive for each common subtree a reference to the according subtree of both original trees.
Exempted in this comparison are similar paths in a tree, we may not be able to detect them certainly in our approach, though it can be expanded on to include this behavior, if not a perfect match of hashes are required.

Though this list of results is extensive, as each single subtree match is included, even if its parent nodes are also contained in both trees.
To enhance the processability of data we filter results in our technique to only contain the topmost subtree matches.
For this, we check for each pair of nodes in a common subtree match if their root hash is also contained within other common subtree matches.
If this is the case, we exclude them from the list of possible topmost subtrees.
Otherwise, we have found the topmost equal node.

Those common subtree still adhere to no specific further restrictions, and can be of any type of node.
We can apply filters later on, as we try to discover possible modifications, to reduce the occurrence of these.
In this filtering and detection of structures, we depend on certain cases which might occur in \gls{AST} matches or tree matching in general. Possible cases are described in more detail in \autoref{equality-in-trees}, some terms defined there are used here to simplify given explanations.

### Detection of Sequences

\begin{figure}[t]
  \includegraphics[width=\textwidth]{images/sequence.pdf}
  \caption[Sequence Example AST]{An example code snippet with an abbreviated tree representation. The colors indicate which node is associated with which expression.}
  \label{figure:sequence}
\end{figure}

Subtrees which are now known to be a duplicate somewhere in the code can occur in a sequence.
In the expression-based \gls{AST} generation of \emph{javaparser} used in the implementation, this is achieved by finding directly adjacent siblings in block statements, we go further into detail in \autoref{implementation}.
The basic algorithm checks if two nodes have the same parent, if this is the case, than the position is checked they have in the children of this node, if they are adjacent we have detected one occurring sequence.
It can be extend on by again finding adjacent nodes to this sequence.

\autoref{figure:sequence} contains an example from the \emph{JUnit5} source code, left is the actual code and on the right is a shortened \gls{AST} representation.
The exemplary sequence, creating two instances of a `String`, is shown in orange on the left in the code snippet, as the first two expressions contained in the method block statement, and on the right as the first two children from the left of the root node.

### Modifications of Sequences
With the help of these found sequences we can suggest actions, like the extraction of most commonly used sequences into separated methods. 
This can be helpful in the scenario of often occurring boilerplate code for initialization of some libraries, or the merging of several methods into a single one.

The merging and extraction can occur in some situations, the first being that all expressions in a complete test are in sequence and also occur as this exact sequence in another test. Important here is the denomination that the first found sequence must be part of a test case, removing the first method without any other restrictions may lead to an incorrect result removing helper methods or similar constructs. We can instead replace the second sequence out in the second test, using the helper method. This can be seen as \emph{Partial Full Equality}.
Second, if the first method found is a test case, then this method can be safely removed. Though this can only be done for type-1 clones and up to some type-2 clones which do not differ in used literals. As removing these test cases on proper type-2 clones might remove test cases which are calling differently parameterized tests.

Third, the aforementioned extraction of sequences. If a sequence can be found in multiple locations, they can be extracted by creating a new method, which we thee reference in the position of the occurrences of the sequence. Methods with this characteristic can be classified in the group of \emph{Partial Equality}.


### Detection of Equal Methods

A rather simple case which might occur is the finding of test cases which are completely equal.
This is independent from found Sequences, as the complete code block of the methods can be matched.
We can find these situations, in case of the common subtree in the matching result is a `Block`, this represents the \emph{Full Equality} of both trees.
Similar to sequences, these equal methods are searched on type-2 clones abstractions, without the omission of literals in the matching.

### Modifications of Equal Methods

Once we have determined equal methods in the changed source code, we check if the found method is replacable by removing the newly added version.
An alteration can be done if both methods are test cases, meaning they are purely used to test some characteristics of the implementation and not being dependent on in other parts of the code.
Exceptions have to be made here if a test is duplicated because it is run under different parameters, for example, different input, then a removal cannot be done without breaking coverage and the content of both test cases is extracted into a helper method which can then be called.

### Detection of General Similarity

As a general measure of similarity we include a catch-all filter to include all matched subtree and calculate the general similarity of two methods based on the amount of nodes contained in the common subtrees relative to the size of the newly added method.

## External Workflow

This section describes the intended workflow for the user of a model application.
We orient the behavior to be easily integrable into the pull request driven forking workflow.
This workflow plans that users who want to submit code to a repository fork it and add their changes to their own version of the repository and later on send a request to take the commits applied by them, back to the original git repository.

Since the final goal of this tool, is to compare two different branches on clones, we have to include other branches and find clones on the \emph{git diff} to new versions of the source code, optimally by integrating with the \gls{VCS}.
For this we use in our prototype \emph{jgit} a java native implementation of the git source code control tool, which additionally to being usable as a CLI tool itself, exposes an API making it possible for us to compare the two different versions of the source code based on their HEAD name, as for example a branch name. Though implementation via other libraries are equally possible. 
This way we integrate well into the workflow of the git tool, we can simply point to two branch references and let the tool create a new commit on the local end of the \emph{Fork's} remote branch.
The new changes can then be reviewed by the author, before accepting them.
They are then pushable to the remote repository.

Little additional setup has to be done, where the git pull requst workflow is used, whereas publicly hosted repositories are used to send changes.
If used, the remote \emph{Changes Branch} is already available and the branch of the pull request target is the \emph{Base Branch}.
For the git patch file workflow, a small change can be added to make the process work without complications, a branch has to be created with the new patches applied to it.
The tool can then be used on this new branch as \emph{Changes Branch}, if the submitted patches are then accepted, the test branch can then be rebased on the \emph{Base Branch}.

For automated alterations of the source code, a sensible approach is the optional automatic application of changes which are then committed onto the \emph{Changes Branch} and push them to the remote repository with an extra deployment key. This gives the advantage of easily being able to revert history, in case any changes have to be removed.
Additionally, this keeps the versions of the code cleanly separated from the automatic modifications and manually authored changes.

Automatic changes can generally be applied for simpler and less complex cases. For more complex cases, such as often occurring subtrees, which are nearly identical, can be reported as a warning to the maintainers to allow a more focused review of the proposed changes.
The output of the tool is parseable, for instance, in the JSON format, to allow processing of results and form them into an automated report for pull request reviews or emails in email threads. Having this modular inclusion into workflows, many scenarios can be realized purely by implementing minimal wrappers around a potential tool.

