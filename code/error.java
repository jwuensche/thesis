// Changes Branch
public void testSingleElementWithStringFactoryRead() throws Exception {
    String json = aposToQuotes("{ 'values': '333' }");
    WrapperWithStringFactoryInList response = MAPPER.readValue(json, WrapperWithStringFactoryInList.class);
    assertNotNull(response.values);
    assertEquals(1, response.values.size());
    assertEquals("333", response.values.get(0).role.Name);
}

// Base Branch
public void testCustomBeanDeserializer() throws Exception {
    // start of the method abbreviated...
    beans = MAPPER.readValue(json, TestBeans.class);
    assertNotNull(beans);
    results = beans.beans;
    assertNotNull(results);
    assertEquals(2, results.size());
    bean = results.get(0);
    assertEquals("", bean.d);
    c = bean.c;
    assertNotNull(c);
    assertEquals(-4, c.a);
    assertEquals(3, c.b);
    bean = results.get(1);
    assertEquals("abc", bean.d);
    c = bean.c;
    assertNotNull(c);
    assertEquals(0, c.a);
    assertEquals(15, c.b);
}
