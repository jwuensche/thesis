@Test
void providesArgumentsWithStringDelimiter() {
    CsvFileSource annotation = csvFileSource()//
            .resources("test.csv")//
            .delimiterString(",")//
            .build();

    Stream<Object[]> arguments = provideArguments(annotation, "foo, bar \n baz, qux \n");

    assertThat(arguments).containsExactly(array("foo", "bar"), array("baz", "qux"));
}

@Test
void ignoresCommentedOutEntries() {
    CsvFileSource annotation = csvFileSource()//
            .resources("test.csv")//
            .delimiter(',')//
            .build();

    Stream<Object[]> arguments = provideArguments(annotation, "foo, bar \n#baz, qux");

    assertThat(arguments).containsExactly(array("foo", "bar"));
}

