@Test
public void testList() {
    List<Integer> bar = new ArrayList<>();
    bar.add(21);
    bar.add(42);
    assert bar.size() == 2;
}

@Test
public void testArrayList() {
    TestObject foo = new TestObject() {
            public test() {
                List<Integer> foo = new ArrayList<>();
                foo.add(21);
                foo.add(42);
                assert foo.size() == 2;
            }
        };
    foo.test();
}
