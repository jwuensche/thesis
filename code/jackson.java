public void testSingleElementWithStringFactoryRead() throws Exception {
     String json = aposToQuotes("{ 'values': '333' }");
     WrapperWithStringFactoryInList response = MAPPER.readValue(json, WrapperWithStringFactoryInList.class);
     assertNotNull(response.values);
     assertEquals(1, response.values.size());
     assertEquals("333", response.values.get(0).role.Name);
}

public void testProblem744() throws Exception {
     Bean744 bean = MAPPER.readValue("{\"name\":\"Bob\"}", Bean744.class);
     assertNotNull(bean.additionalProperties);
     assertEquals(1, bean.additionalProperties.size());
     assertEquals("Bob", bean.additionalProperties.get("name"));
}
