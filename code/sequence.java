@ParameterizedTest(name = "Indexed Arguments and ArgumentsAccessor: {arguments}")
@CsvSource({ "1, 2, 3, 4, 5, 6, 7, 8, 9, 10" })
void indexedArgumentsAndArgumentsAccessor(int num1, int num2, ArgumentsAccessor arguments) {
    assertEquals(1, num1);
    assertEquals(2, num2);
    assertEquals(55, IntStream.range(0, arguments.size()).map(i -> arguments.getInteger(i)).sum());
}

@ParameterizedTest(name = "Indexed Arguments, ArgumentsAccessor, and TestInfo: {arguments}")
@CsvSource({ "1, 2, 3, 4, 5, 6, 7, 8, 9, 10" })
void indexedArgumentsArgumentsAccessorAndTestInfo(int num1, int num2, ArgumentsAccessor arguments,
        TestInfo testInfo) {

    assertEquals(1, num1);
    assertEquals(2, num2);
    assertEquals(55, IntStream.range(0, arguments.size()).map(i -> arguments.getInteger(i)).sum());
    assertThat(testInfo.getDisplayName()).startsWith("Indexed Arguments, ArgumentsAccessor, and TestInfo");
}
