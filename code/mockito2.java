// Base Branch
@Test
public void can_define_class_in_closed_module() throws Exception {
    assumeThat(Plugins.getMockMaker() instanceof InlineByteBuddyMockMaker, is(false));
    Path jar = modularJar(true, true, false);
    ModuleLayer layer = layer(jar, false);
    ClassLoader loader = layer.findLoader("mockito.test");
    Class<?> type = loader.loadClass("sample.MyCallable");
    ClassLoader contextLoader = Thread.currentThread().getContextClassLoader();
    Thread.currentThread().setContextClassLoader(loader);
    try {
        Class<?> mockito = loader.loadClass(Mockito.class.getName());
        @SuppressWarnings("unchecked")
        Callable<String> mock = (Callable<String>) mockito.getMethod("mock", Class.class).invoke(null, type);
        Object stubbing = mockito.getMethod("when", Object.class).invoke(null, mock.call());
        loader.loadClass(OngoingStubbing.class.getName()).getMethod("thenCallRealMethod").invoke(stubbing);
        boolean relocated = !Boolean.getBoolean("org.mockito.internal.noUnsafeInjection") && ClassInjector.UsingReflection.isAvailable();
        String prefix = relocated ? "sample.MyCallable$MockitoMock$" : "org.mockito.codegen.MyCallable$MockitoMock$";
        assertThat(mock.getClass().getName()).startsWith(prefix);
        assertThat(mock.call()).isEqualTo("foo");
    } finally {
        Thread.currentThread().setContextClassLoader(contextLoader);
    }
}
