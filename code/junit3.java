// Changes Branch
@Test
void readsLineFromDefaultMaxCharsFileWithDefaultConfig(@TempDir Path tempDir) throws Exception {
    Path csvFile = writeClasspathResourceToFile("/default-max-chars.csv", tempDir.resolve("default-max-chars.csv"));
    CsvFileSource annotation = //
    csvFileSource().encoding(//
    "ISO-8859-1").resources(//
    "/default-max-chars.csv").files(//
    csvFile.toAbsolutePath().toString()).build();
    Stream<Object[]> arguments = provideArguments(new CsvFileArgumentsProvider(), annotation);
    assertThat(arguments).hasSize(2);
}

// Base Branch
@Test
void readsFromClasspathResourcesAndFiles(@TempDir Path tempDir) throws Exception {
    Path csvFile = writeClasspathResourceToFile("/single-column.csv", tempDir.resolve("single-column.csv"));
    CsvFileSource annotation = //
    csvFileSource().encoding(//
    "ISO-8859-1").resources(//
    "/single-column.csv").files(//
    csvFile.toAbsolutePath().toString()).build();
    Stream<Object[]> arguments = provideArguments(new CsvFileArgumentsProvider(), annotation);
    assertThat(arguments).hasSize(2 * 5);
}
