public String foo() {
    return "foo";
},

@Test
public void shouldReadConfigurationClassFromClassPath() {
    ConfigurationAccess.getConfig().overrideDefaultAnswer(new Answer<Object>() {

        public Object answer(InvocationOnMock invocation) {
            return "foo";
        }
    });
    IMethods mock = mock(IMethods.class);
    assertEquals("foo", mock.simpleMethod());
}
