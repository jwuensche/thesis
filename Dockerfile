FROM pandoc/latex 
RUN apk add make grep sed librsvg evince hunspell biber inotify-tools ttf-liberation
RUN tlmgr update --self && tlmgr install titlesec chngcntr todonotes glossaries mfirstuc xfor datatool libertine dejavu
# COPY ./ /data/
