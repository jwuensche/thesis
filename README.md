# Analysis and Merging of Similar Test Cases in Forked Software Systems

This repository contains my ongoing bachelor thesis.
Research and implementation is still ongoing, so expect to find scratchpad like notes in here too.

## Requirements

Dependencies to build this document are:
```
pdflatex
biber
hunspell
pandoc
```

If installed on your system you can also use `inotifywait` to build once changes occur.

## Usage

To build this thesis run:
```
$ make
```

To continously build once a file changes use:
```
$ make run
```
and your configured pdf-viewer will open.


